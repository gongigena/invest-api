const USER_EXIST = {
  Error: 'El mail ya ha sido usado',
};
const USER_REGISTERED = {
  Message: 'Usuario se ha registrado correctamente',
};

const USER_OR_PASSWORD = {
  Error: 'El mail o la constraseña son incorrectos',
};
const USER_LOGGED = {
  Message: 'Usuario se ha logeado correctamente',
};
const CONNECTION = {
  Error: 'No se pudo conectar a la base de datos',
};

module.exports = {
  insertUser: function(json, mc, url) {
    // Create a Promise to use .then
    return new Promise((resolve, reject) => {
      mc.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      function(err, database) {
        // If it already exist , does nothing
        if (err){
          reject(CONNECTION);
        } else {
          var dbo = database.db('users');
          dbo.collection('userInfo').insertOne(json, (error, res) => {
            if (error){
              reject(USER_EXIST);
            } else {
              resolve(USER_REGISTERED);
            }
          });
        }
      });
    });
  },
  checkUser: function(json, mc, url){
    // Create a Promise to use .then
    return new Promise((resolve, reject) => {
      mc.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      function(err, database) {
        // If it already exist , does nothing
        if (err){
          reject(CONNECTION);
        } else {
          var dbo = database.db('users');
          dbo.collection('userInfo').findOne(json, (error, res) => {
            if (res === null || error){
              reject(USER_OR_PASSWORD);
            } else if (res['password'] !== json['password']){
              reject(USER_OR_PASSWORD);
            } else {
              resolve(USER_LOGGED);
            }
          });
        }
      });
    });
  },

};
