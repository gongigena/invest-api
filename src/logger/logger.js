const {format, createLogger, transports} = require('winston');

module.exports = createLogger({
  format: format.combine(
    format.simple(),
    format.timestamp(),
    format.printf(
      info =>
        `[${info.timestamp}] | ${info.message}`
    )
  ),
  transports: [
    new transports.File({
      maxsize: 5120000,
      maxFiles: 2,
      filename: 'src/logger/invest-api.log',
    }),
    new transports.Console({
      level: 'debug',
    }),
  ],

});
