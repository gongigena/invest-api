const fetch = require('node-fetch');

// Need to use an environment variable
// const API_Key = process.env.AKEY;
const API_Key = 'X86NOH6II01P7R24';
const URL = 'https://www.alphavantage.co/query?';

class AVApi {
  constructor(){
    this.apikey = API_Key;
    this.url = URL;
  }
  async getStock(company){
    const fetchUrl =
    `${this.url}function=TIME_SERIES_DAILY&symbol=${company}&apikey=${this.apiKey}`;
    console.log(fetchUrl);
    return fetch(fetchUrl).then(
      (res) => {
        return res.json();
      }
    );
  }
};

module.exports = AVApi;
