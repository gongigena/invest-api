const WRONG_NAME = {
  Error: 'El símbolo de la empresa no existe',
};

module.exports = {

  handle_response: function(json, company){

    var today = get_date(0);
    var yesterday = get_date(1);

    var answer = {
      symbol: company,
      value: '',
      previous: '',
      change_percent: '',
      change_value: '',
      color_code: '',
    };
    
    // In case there company symbol is wrong
    if (json['Error Message']){
      return WRONG_NAME;
    }
    if(typeof (json['Time Series (Daily)'][today]) === 'undefined'){
      return {
        "ERROR" : 'No hay datos del día de hoy',
      };
    }
    var value = json['Time Series (Daily)'][today]['4. close'];
    answer['value'] = value;

    let previous = json['Time Series (Daily)'][yesterday]['4. close'];
    answer['previous'] = previous;


    if (previous <= value){
      answer['color_code'] = 'green';
    } else {
      answer['color_code'] = 'red';
    }

    answer['change_percent'] = ((value / previous - 1) * 100).toFixed(3);

    answer['change_value'] = (value - previous).toFixed(3);

    return answer;
  },
};

function get_date(day){
  var today = new Date();
  today.setDate(today.getDate() - day);
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + '-' + mm + '-' + dd;
  return today;
}
