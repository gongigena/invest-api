const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();

// Settings

app.set('port', 3000);
app.set('json spaces', 2);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// Routes
app.use('/api', require('./routes/routes'));

// Startimg Server
app.listen(app.get('port'));
