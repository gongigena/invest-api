const { Router } = require('express');
const router = Router();
const jwt = require('jsonwebtoken');
const stock = require('./stocks');

//  Alpha Vantage Api
const AVApi = require('../AVApi/AVApi');
const av_api = new AVApi;

// Mongo client
const mc = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27018/';
const database = require('../database/bd');

// Logger
const logger = require('../logger/logger');

// Key for JWT
// const secretkey = process.env.JWTKEY;
const secretkey = 'secretkey';

// Messages
const ERROR = {
  Error: 'Hubo algún problema',
};

const PARAMETERS = {
  Error: 'Faltan parámetros',
};

router.get('/stock', verify_Token, function(req, res){             

  jwt.verify(req.token, secretkey, async function(err, Data){
    if(err){
      logger.info("api/stock | GET | "+"company = " + req.query.company
                                      +" | VerifyFailed");                
      res.sendStatus(403);
    } else {
      await database.checkUser(Data['user'], mc, url).then(async function(){

        let company = req.query.company;
        await av_api.getStock(company)
          .then((json) => {
            let response = stock.handle_response(json, company);
            logger.info("api/stock | GET | "+"company = " + req.query.company);                            
            res.send(response);
          })
          // If the company symbol is wrongs 
          .catch((err) => {
            console.log(err); 
            logger.info("api/stock | GET | "+"company = " + req.query.company
                                            + " | WrongCompanySymbol");
            res.send(ERROR); 
          });

        // If the user or password is incorrect
      }).catch(err => {
        logger.info("api/stock | GET | "+"company = " + req.query.company
                                        + " | WrongUserOrPassword");
        res.send(err);
      }
      );
    
    }
  });
});


function verify_Token(req, res, next){

  // Authorization : Bearer <token>

  const header = req.headers['authorization'];

  if (typeof (header) !== 'undefined'){

    const token = header.split(' ')[1];
    req.token = token;
    next();

  } else {
    logger.info("api/stock | GET | NoToken");    
    // There is no Token, access forbidden
    res.sendStatus(403);
  }

}

router.post('/user/register', async function(req, res){              
  
  // Check if all parameters exist
  if (!req.query.email || !req.query.name || !req.query.password){
    logger.info("api/user/register | POST | InsufficientParams");
    res.send(PARAMETERS);
  } else {

    const user = {
      _id: req.query.email,
      password: req.query.password,
      name: req.query.name,
    };

    // Check if the user can be created
    await database.insertUser(user, mc, url)
      .then(json => {
        logger.info("api/user/register | POST | "
              +"email = " + req.query.email
              +", name = " + req.query.name
              +", password = " + req.query.password
              +" | UserCreated");  
        res.send(json);
      
      }).catch(err => {
        logger.info("api/user/register | POST | "
              +"email = " + req.query.email
              +", name = " + req.query.name
              +", password = " + req.query.password
              +" | UserNOtCreated");  
        res.send(err)}
      );

  }

});


router.post('/user/login', async function(req, res){

  if (!req.query.email || !req.query.password){
    logger.info("api/user/register | POST | InsufficientParams");    
    res.send(PARAMETERS);
  } else {

    const user = {
      _id: req.query.email,
      password: req.query.password,
    };

    // Check if the user exist in the dataBase
    await database.checkUser(user, mc, url).then(json => {
      // If the user exist returns a message
      // with a Token
      let message = json['Message'];
      console.log(json);

      jwt.sign({user}, secretkey, (err, token) => {
        if (err){
          logger.info("api/user/login | POST | SignFailed");
          res.send(ERROR);
        } else {
          logger.info("api/user/login | POST | "
              +"email = " + req.query.email
              +", password = " + req.query.password
              +" | UserLogged");
          res.send({message, token});
        }
      });

    }).catch(err => {
      logger.info(" api/user/login | POST | "
              +"email = " + req.query.email
              +", password = " + req.query.password
              +" | WrongUserOrPassword");
      res.send(err)
    });

  }
});


module.exports = router;
